let http = require("http");

// Mock database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@gmail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@gmail.com"
	}
];

http.createServer(function (request, response) {
	// Route for returning all items
	if(request.url == "/users" && request.method == "GET"){
		// sets response output to JSON data type
		response.writeHead(200, {'Content-Type': 'application/json'});

		response.write(JSON.stringify(directory));

		response.end();
	}

	// Route for creating data
	if(request.url == "/users" && request.method == "POST"){
		let requestBody = '';

		request.on('data', function(data){

			// requestBody = requestBody + data;
			requestBody += data;
		});

		// response end step - only runs after the request has been completely sent
		request.on('end', function(){
			// check if at this point the requestBody is of data type STRING
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Create a new object representing the new mock database record
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			// Add the mew user to the mock database
			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type': 'application/json'});

			response.write(JSON.stringify(newUser));

			response.end();
		});

	}
}).listen(4000);

console.log(`Server running at localhost:4000`);